# Header
FROM ubuntu:latest

# Install dependencies
RUN apt-get update && apt-get install -y \
    autoconf \
    g++ \
    gcc \
    git \
    libhts-dev \
    libhts1 \
    make \
    libbz2-dev \
    libcrypto++-dev \
    libcurl4-gnutls-dev \
    liblzma-dev \
    zlib1g \
    zlib1g-dev

RUN git clone https://github.com/samtools/htslib && \
    cd htslib && \
    autoheader && \
    autoconf && \
    ./configure && \
    make && \
    make install

# Copy in source code
COPY . /src/

WORKDIR /src/

RUN make HTSSRC=../htslib && make test && make install
